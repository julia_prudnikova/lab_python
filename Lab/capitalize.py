#! /usr/bin/env python
import sys

if len(sys.argv) == 3:
    flag = False
    try:
        f1 = open(sys.argv[1])
        flag = True
    except:
        print 'Invalid File'
    if flag:
        f2 = open(sys.argv[2], 'w+')
        for line in f1:
            f2.write(line.upper())
        f1.close()
        f2.close()
else:
    print 'Not a valid number of files'