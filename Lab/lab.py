# -*- coding: cp1251 -*-

import sys
import math

print("(ax^2 + bx + c = 0):")
a = float(raw_input("a = "))
b = float(raw_input("b = "))
c = float(raw_input("c = "))
 
discr = b*b - 4 * a * c;
print("D = %.2f" % discr)
if discr > 0:
	x1 = (-b + math.sqrt(discr)) / (2 * a)
	x2 = (-b - math.sqrt(discr)) / (2 * a)
	print("x1 = %.2f \nx2 = %.2f" % (x1, x2))
elif discr == 0:
	x = -b / (2 * a)
	print("x = %.2f" % x)
else:
	print("No roots")