#! /usr/bin/env python

import sys


if len(sys.argv) > 1:
    i = 0
    for f in sys.argv:
        if i > 0:
            with open(sys.argv[i]) as f:
                print 'File - ' + sys.argv[i] + '; Strok - ' + int(sum(1 for _ in f)).__str__()
            f.close()
        i += 1
else:
    print 'No file to count'